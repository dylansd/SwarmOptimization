package com.dksd.optimization;

import java.util.List;

/**
 *
 * @author dscottdawkins
 */
public interface FitnessFunction {

   double calcFitness(Particle p);

   List<Domain> getDomains();

   Double getEstimatedMinimum();

}
