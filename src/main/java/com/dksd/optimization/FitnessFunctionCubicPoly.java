package com.dksd.optimization;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dscottdawkins
 */
public class FitnessFunctionCubicPoly implements FitnessFunction {

	private final double[] xi;
	private final double [] yi;
	
	public FitnessFunctionCubicPoly(final double[] xi, final double [] yi) {
		this.xi = xi;
		this.yi = yi;
	}

	public double calcFitness(Particle p) {
	   /*System.out.println("x values: ");
	   for (int i = 0; i < xi.length; i++) {
	      System.out.print(xi[i]+",");
	   }
	   System.out.println();
	   System.out.println("y values: ");
	   for (int i = 0; i < xi.length; i++) {
         System.out.print(yi[i]+",");
      }
	   System.out.println();*/
		double error = 0;
		for (int i = 0; i < xi.length; i++) {
			error = error
					+ Math.abs(yi[i]
							- fox(p.getGene().gene[0], p.getGene()
									.gene[1], p.getGene().gene[2], p
									.getGene().gene[3], xi[i]));
		}
		return error;
	}

	private double fox(double a, double b, double c, double d, double x) {
		double x2 = x * x;
		double x3 = x2 * x;
		return a * x2 + b * x + c;
		//return a * x3 + b * x2 + c * x + d;
	}

	public int getDimension() {
		return 4;
	}

	public List<Domain> getDomains() {
		List<Domain> domains = new ArrayList<>(getDimension());
		for (int i = 0; i < domains.size(); i++) {
			domains.add(new Domain("x",-100, 100));
		}
		return domains;
	}

	@Override
	public Double getEstimatedMinimum() {
		return null;
	}


}
