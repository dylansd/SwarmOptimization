package com.dksd.optimization;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dscottdawkins
 */
public class FitnessFunctionParabolic implements FitnessFunction {

	public double calcFitness(Particle p) {
		return p.getGene().gene[0] * p.getGene().gene[0];
	}

	public List<Domain> getDomains() {
		List<Domain> domains = new ArrayList<>(1);
		for (int i = 0; i < 1; i++) {
			domains.add(new Domain("x", -1000, 1000));
		}
		return domains;
	}

	@Override
	public Double getEstimatedMinimum() {
		return null;
	}

}
