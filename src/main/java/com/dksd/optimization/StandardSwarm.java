package com.dksd.optimization;

import java.util.Collection;

/**
 * Simple concurrent swarm that updates all particles simultaneously.
 *
 * @author dscottdawkins
 */
public class StandardSwarm extends AbstractSwarm implements Swarm {

    /**
     * Constructs a swarm given the fitness function, the number of particles and potentially some starting particles.
     *
     * @param ff fitness function
     * @param numParticles number of particles in this swarm
     */
    public StandardSwarm(FitnessFunction ff, int numParticles, int maxSteps) {
        super(ff, numParticles, maxSteps);
    }

    /**
     * Constructs a swarm given the fitness function, the number of particles and potentially some starting particles.
     *
     * @param ff fitness function
     * @param numParticles number of particles in this swarm
     * @param seedParticles beginning particles that could be good guesses or previous solutions
     */
    public StandardSwarm(FitnessFunction ff, int numParticles, Collection<Particle> seedParticles, int maxSteps) {
        super(ff, numParticles, seedParticles, maxSteps);
    }

    /**
     * Updates all particles in the swarm based on global and neighbourhood bests.
     *
     * @return true if the swarm converged, false otherwise
     * @throws InterruptedException if the count down latch was interrupted
     */
    public boolean step() throws InterruptedException {
        long now = System.currentTimeMillis();
        for (Particle particle : this.particles) {
            updateParticle(step.get(), now, particle);
        }
        return isConverged(step.get(), now);
    }
}
