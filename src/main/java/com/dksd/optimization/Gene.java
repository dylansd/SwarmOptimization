package com.dksd.optimization;

import java.util.List;

/**
 * Simple array of doubles to represent a possible solution as measured by the fitness function.
 *
 * @author dscottdawkins
 */
public class Gene {

    private final List<Domain> domains;
    /**
     * Array of values representing the solution.
     */
    public double[] gene;

    public Gene(List<Domain> domains) {
        this.domains = domains;
        gene = new double[domains.size()];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        for (int i = 0; i < domains.size(); i++) {
            result.append(domains.get(i).getName());
            result.append(" = ");
            result.append(gene[i]);
            result.append(", ");
        }
        return result.toString();
    }

    public void copyInto(Gene pBest) {
        for (int i = 0; i < gene.length; i++) {
            pBest.gene[i] = gene[i];
        }
    }
}
