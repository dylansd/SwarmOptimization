package com.dksd.optimization;

import java.security.SecureRandom;
import java.util.Random;

public class Domain {
    private static Random rand = new SecureRandom();
    private final String name;
    private final double start;
    private final double end;
    private final double scalar;

    public Domain(String name, double start, double end) {
        this.name = name;
        this.start = start;
        this.end = end;
        this.scalar = Math.abs(end - start);
    }

    public double pickRandom() {
        return start + (rand.nextDouble() * (end - start));
    }

    public double getEnd() {
        return end;
    }

    public double getStart() {
        return start;
    }

    public String getName() {
        return name;
    }

    public double getScalarMagnitude() {
        return scalar;
    }
}