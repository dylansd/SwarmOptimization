package com.dksd.optimization;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.SplittableRandom;

/**
 * Represents a vector in space that flies through the search space with a velocity,
 * following the global and personal bests found so far.
 *
 * @author dscottdawkins
 */
public class Particle {

    /** Pay VERY careful attention to the random generator. There are better ones out there. */
    private static SplittableRandom rand = new SplittableRandom();

    private Gene pBest;
    private Gene particle;
    //TODO possible idea is introduce the idea of quantum tunnelling here.
    //So velocity or momentum gets bigger as position improves,
    //And randomly the position of the particle can jump randomly
    private Gene velocity;
    private double fitness;
    private double pfitness;
    private final FitnessFunction ff;

    private double w = 0.6;
    private double gbestconst = 0.9;
    private double pbestconst = 0.75;
    private final List<Domain> domains;

    /** Ctor. */
    public Particle(FitnessFunction ff) {
        this.ff = ff;
        this.domains = ff.getDomains();
        this.particle = new Gene(domains);
        this.velocity = new Gene(domains);
        this.pBest = new Gene(domains);
    }

    public void init(List<Domain> sadomains) {
        for (int i = 0; i < particle.gene.length; i++) {
            double rndv = sadomains.get(i).pickRandom();
            particle.gene[i] = rndv;
            velocity.gene[i] = 0;
            pBest.gene[i] = particle.gene[i];
        }
        pfitness = ff.calcFitness(this);
    }

    public double update(Gene gBest) {
        //intro fork join pool here.
        for (int i = 0; i < particle.gene.length; i++) {
            double r1 = pickRandom();
            double r2 = pickRandom();
            double v = w * velocity.gene[i] + pbestconst * r1 * (pBest.gene[i] - particle.gene[i])
                    + gbestconst * r2 * (gBest.gene[i] - particle.gene[i]);

            Domain domain = domains.get(i);
            v = constrainMaxVelocity(v, domain.getScalarMagnitude());
            velocity.gene[i] = v;
            particle.gene[i] = particle.gene[i] + v;
            constrainParticleToDomain(i, domain);
        }
        fitness = ff.calcFitness(this);
        if (fitness < pfitness) {
            this.getGene().copyInto(pBest);
            pfitness = fitness;
        }
        return fitness;
    }

    private void constrainParticleToDomain(int index, Domain domain) {
        if (particle.gene[index] > domain.getEnd()) {
            particle.gene[index]  = domain.getEnd();
        }
        if (particle.gene[index] < domain.getStart()) {
            particle.gene[index] = domain.getStart();
        }
    }

    private double constrainMaxVelocity(double vel, double domainLength) {
        if (Math.abs(vel) > domainLength) {
            vel = 0.8 * domainLength;
        }
        return vel;
    }

    private static double pickRandom() {
        return rand.nextDouble();
    }

    public double getFitness() {
        return fitness;
    }

    public Gene getGene() {
        return particle;
    }
}
