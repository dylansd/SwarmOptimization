package com.dksd.optimization;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Simple concurrent swarm that updates all particles simultaneously.
 *
 * @author dscottdawkins
 */
public class StandardConcurrentSwarm extends AbstractSwarm implements Swarm {

    BlockingQueue<Runnable> linkedBlockingDeque = null;
    ExecutorService executorService = null;

    /**
     * Constructs a swarm given the fitness function, the number of particles and potentially some starting particles.
     *
     * @param ff fitness function
     * @param numParticles number of particles in this swarm
     * @param seedParticles beginning particles that could be good guesses or previous solutions
     */
    public StandardConcurrentSwarm(FitnessFunction ff, int numParticles, Collection<Particle> seedParticles, int maxSteps) {
        super(ff, numParticles, seedParticles, maxSteps);
        initExecutors(numParticles);
    }

    private void initExecutors(int numParticles) {
        this.linkedBlockingDeque = new LinkedBlockingDeque<>(
                numParticles);
        this.executorService = new ThreadPoolExecutor(1, 20, 30,
                TimeUnit.SECONDS, linkedBlockingDeque,
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    public StandardConcurrentSwarm(FitnessFunction ff, int numParticles, int maxSteps) {
        super(ff, numParticles, maxSteps);
        initExecutors(numParticles);
    }

    /**
     * Updates all particles in the swarm based on global and neighbourhood bests.
     *
     * @return true if the swarm converged, false otherwise.
     */
    public boolean step() {
        long now = System.currentTimeMillis();
        int cStep = step.get();
        for (Particle particle : this.particles) {
            Runnable runnable = () -> {
                updateParticle(cStep, now, particle);
            };
            executorService.submit(runnable);
        }
        boolean converged = isConverged(cStep, now);
        if (isConverged(cStep, now)) {
            executorService.shutdown();
        }
        return converged;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }
}
