package com.dksd.optimization;

import java.util.List;

/**
 *
 * @author dscottdawkins
 */
public class FitnessFunctionPowerFromVel implements FitnessFunction {

    private double power;
    private double cda;
    private double totalMass;
    private double slope;
    private double estimatedMinimum;

    @Override
    public double calcFitness(Particle p) {
        Gene gene = p.getGene();
        return Math.abs(power - calcPower(gene.gene[0], cda, totalMass, slope));
    }

    @Override
    public List<Domain> getDomains() {
        return null;//new double[50]; TODO fix this
    }

    @Override
    public Double getEstimatedMinimum() {
        return null;
    }

    public static double calcPower(double vms, double cda, double totalMass, double slope) {
        double driveEfficiency = 0.96;
        return (getPowerDrag(vms, cda)
                + getPowerRolling(vms, totalMass)
                + getPowerClimbing(vms, totalMass, slope)) / driveEfficiency;
    }

    private static double getPowerRolling(double vms, double riderMass) {
        return vms * riderMass * 9.8 * 0.005 /*Crr*/;
    }

    private static double getPowerClimbing(double vms, double riderMass, double slope) {
        return vms * riderMass * 9.8 * slope;
    }

    private static double getPowerDrag(double vms, double cda) {
        return 0.5 * vms * vms * vms * cda;
    }

}
