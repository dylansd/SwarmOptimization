package com.dksd.optimization;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractSwarm implements Swarm {

    private static final int GBEST_IDLE_TIME = 5000;
    private static final double GBEST_IMPROVE_EPSILON = -0.00001;
    protected final Collection<Particle> particles;
    protected final FitnessFunction ff;
    protected volatile Gene gBest;
    protected final int maxSteps;
    protected volatile double gbestFitness;
    protected volatile int lastGbestMoveStep = 0;
    protected AtomicLong lastGbestMove = new AtomicLong(2*System.currentTimeMillis());
    protected AtomicInteger step = new AtomicInteger();
    private Random rand = new Random();
    private final List<Domain> domains;

    /** See the full constructor description. */
    public AbstractSwarm(FitnessFunction ff, int numParticles, int maxSteps) {
        this(ff, numParticles, Collections.emptyList(), maxSteps);
    }

    /**
     * Constructs a swarm given the fitness function, the number of particles and potentially some starting particles.
     *  @param ff fitness function
     * @param numParticles number of particles in this swarm
     * @param seedParticles beginning particles that could be good guesses or previous solutions
     * @param maxSteps
     */
    public AbstractSwarm(FitnessFunction ff, int numParticles, Collection<Particle> seedParticles, int maxSteps) {
        this.ff = ff;
        this.maxSteps = maxSteps;
        this.domains = ff.getDomains();
        this.particles = new ArrayList<>(numParticles + seedParticles.size());
        this.particles.addAll(seedParticles);
        this.gBest = new Gene(domains);
        for (int i = 0; i < numParticles; i++) {
            Particle particle = new Particle(ff);
            particles.add(particle);
            particle.init(domains);
            if (i == 0) {
                particle.getGene().copyInto(gBest);
                gbestFitness = ff.calcFitness(particle);
            }
        }
    }

    @Override
    public void printGbest() {
        for (int i = 0 ; i < getGbest().gene.length; i++) {
            String name = domains.get(i).getName();
            double val = getGbest().gene[i];
            System.out.print(name + ": " + val + ", ");
        }
        System.out.println();
    }

    public Gene getGbest() {
        return gBest;
    }

    public double getGbestFitness() {
        return gbestFitness;
    }

    protected boolean isConverged(int currStep, long now) {
        if (step.incrementAndGet() > maxSteps) {
            return true;
        }
        if (ff.getEstimatedMinimum() != null && this.gbestFitness > ff.getEstimatedMinimum() + 0.001) {
            return false;
        }
        if (currStep - lastGbestMoveStep  > 100) {
            return true;
        }
        long gbest_last_update = now - lastGbestMove.get();
        return gbest_last_update > GBEST_IDLE_TIME;
    }

    protected void updateParticle(int step, long now, Particle particle) {
        double partFitness = particle.update(getGbest());
        double diff = partFitness - gbestFitness;
        if (diff < 0) {
            synchronized (gBest) {
                particle.getGene().copyInto(gBest);
                gbestFitness = partFitness;
                //TODO what if we perturb the best particle with a random tiny vector
                // to see if it improves it's position.
                for (int i = 0; i < particle.getGene().gene.length; i++) {
                    double rnd = rand.nextDouble() * 0.001;
                    double cv = particle.getGene().gene[i];
                    particle.getGene().gene[i] = cv + rnd;
                }
            }
            lastGbestMove.set(now);
            lastGbestMoveStep = step;
        } else if (diff == 0.0) {
            if (rand.nextDouble() < 0.1) {
                particle.init(domains);
            }
        }
    }

    @Override
    public Collection<Particle> getParticles() {
        return particles;
    }

}
