package com.dksd.optimization;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Tests the particle and how it flies through the search space.
 *
 * @author dscottdawkins
 */
public class ParticleTest {

    /** Class under test. */
    private Particle particle;

    /** Fitness function to evaluate the particles fitness. */
    private FitnessFunction ff;

    /** Sets up the test. */
    @Before
    public void setUp() throws Exception {
        ff = new FitnessFunctionParabolic();
        particle = new Particle(ff);
    }


    @Test
    public void initParticleTest() {
        Domain domain = new Domain("name", 0, 1);
        for (int i = 0; i < 1000; i++) {//test lots of times
            particle.init(Collections.singletonList(domain));
            if (particle.getGene().gene[0] > domain.getEnd() ||
                    particle.getGene().gene[0] < domain.getStart()) {
                Assert.fail("Particle initialization value cannot be outside the set domain [-1,1]");
            }
        }
    }

    @Test
    public void particleShouldSeeGbestOnFirstStepTest() {
        List<Domain> sadomains = new ArrayList<>();
        sadomains.add(new Domain("test", 0, 1));
        Particle gbest = new Particle(ff);
        gbest.init(sadomains);
        gbest.getGene().gene[0] = 0.0;//Set to optimal value
        particle.update(gbest.getGene());

        //Should have converged here to zero as that gives the smallest fitness value.
        Assert.assertEquals(0, particle.getGene().gene[0], 0.0001);
        Assert.assertEquals(0, particle.getFitness(), 0.0001);
    }
}