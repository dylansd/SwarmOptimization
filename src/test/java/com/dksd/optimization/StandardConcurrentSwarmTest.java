package com.dksd.optimization;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author dscottdawkins
 */
public class StandardConcurrentSwarmTest {

    private Swarm swarm;

    private FitnessFunction ff;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        ff = new FitnessFunctionParabolic();
        swarm = new StandardSwarm(ff, 1000, 1000);
    }

    @Test
    public void stepUntilConvergedTest() throws InterruptedException {
        for (int i = 0; i < 100000; i++) {
            swarm.step();
        }
        System.out.println(swarm.getGbest());
    }
}